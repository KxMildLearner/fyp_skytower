﻿using System;
using System.Collections;
using UnityEngine;
using GameSystem.Events;
using UnityEngine.UI;

public class GrabItemController : MonoBehaviour
{
    [SerializeField] private BaseMonsterInfo CharacterData;
    [SerializeField] private PlayerInputSO _inputControl;

    private float HoldBoxJumpForceMultiplier => CharacterData.HoldBoxJumpForceReduction;

    //Flying Monster Exclusive
    float grabPlayerDuration => CharacterData.GrabPlayerDuration;
    float grabPlayerRadius => CharacterData.GrabPlayerRadius;

    [Header("COMPULSORY Components")]
    [SerializeField] Transform grabDetect;
    [SerializeField] float rayDistance;
    [SerializeField] LayerMask interactableLayer;
    [SerializeField] Collider2D groundCheckCollider;
    [SerializeField] Transform characterSprite;

    //Internally Event
    private event Action<GameObject> OnGrabEvent;
    [SerializeField] DetectionBehaviour detectionBrain;

    [Header("Grab Player Event")]
    [SerializeField] GameObjectEvent OnGrabPlayer;
    [SerializeField] VoidEvent OnReleasePlayer;
    [SerializeField] VoidEvent OnSwitchBackPlayer;
    [SerializeField] Slider staminaSlider;
    
    float facingDirection = 0;      //determine which way the model is facing

    GameObject grabbedGameObject;
    Transform itemParent;

    bool isGrabbingItem;
    bool isGrabbingPlayer;
    bool pauseInteraction;

    private void Update()
    {
        if(_inputControl.GetGrabInput())
        {
            if (GroundMonster())
                GroundUnitCheckAndGrabItem();

            if (FlyingMonster())
                FlyingUnitCheckAndGrabItem();
        }
    }

    private void OnEnable()
    {
        OnGrabEvent += GrabAndReleaseItem;
    }

    private void OnDisable()
    {
        OnGrabEvent -= GrabAndReleaseItem;
    }

    private void DetermineFacingDirection(Collider2D collider)
    {
        facingDirection = (collider.bounds.size.x / 2) * GetComponent<MovementController>().IsFacingRight();
    }

    private void GroundUnitCheckAndGrabItem()
    {
       //When hands are empty and check for item to grab

        RaycastHit2D grabCheck = Physics2D.Raycast(grabDetect.position, Vector2.right * characterSprite.lossyScale, rayDistance, interactableLayer);

        if (grabCheck.collider && grabCheck.collider.gameObject.TryGetComponent(out ItemController item))
        {
            //Universal Grab Object Methods that are compulsory
            DetermineFacingDirection(grabCheck.collider);

            switch (item.ItemInfo.ItemType)
            {
                case ItemTypeList.Box:
                    //var boxObj = grabCheck.collider.GetComponent<BoxBehaviour>();
                    if (CharacterActionManager.ReturnCurrentCharacter().IsGrounded())
                    {
                        OnGrabEvent?.Invoke(grabCheck.collider.gameObject); //Equivalent to "GrabItem(grabCheck.collider.gameObject)"
                        return;
                    }
                    break;
            }
        }

        //When is grabbing item, can release item no matter what
        if(isGrabbingItem && CharacterActionManager.ReturnCurrentCharacter().IsGrounded())
        {
            OnGrabEvent?.Invoke(null); 
        }
    }

    private void FlyingUnitCheckAndGrabItem()
    {
        Collider2D[] playerCollider = Physics2D.OverlapCircleAll(grabDetect.transform.position, grabPlayerRadius, interactableLayer);

        foreach(Collider2D detectedItem in playerCollider)
        {
            if (detectedItem.gameObject != gameObject)
            {
                switch(detectedItem.tag)
                {
                    case "Player":
                        if (pauseInteraction)
                            return;

                        if (!isGrabbingPlayer)
                        {
                            //If the player goes into the range of flying unit's grab range and grab the bird
                            DetermineFacingDirection(detectedItem);
                            OnGrabPlayer.Raise(this.gameObject);
                            OnGrabEvent?.Invoke(detectedItem.gameObject);
                            isGrabbingPlayer = !isGrabbingPlayer;
                            StartCoroutine(ReleasePlayerAfterTimer());
                            StartCoroutine(PreventSpam());
                        }
                        else
                        {
                            ReleasePlayer();
                            staminaSlider.gameObject.SetActive(false);
                        }
                        break;
                    case "Key":
                        OnGrabEvent?.Invoke(detectedItem.gameObject);

                        if (isGrabbingItem)
                        {
                            detectedItem.gameObject.GetComponentInChildren<Animator>().enabled = false;
                            detectedItem.gameObject.GetComponentInChildren<SpriteRenderer>().sortingOrder = -1;
                            detectedItem.gameObject.transform.GetChild(0).localPosition = new Vector3(0f, 0f, 0f);
                        }
                        else
                        {
                            detectedItem.gameObject.GetComponentInChildren<Animator>().enabled = true;
                            detectedItem.gameObject.GetComponentInChildren<SpriteRenderer>().sortingOrder = 20;
                            detectedItem.gameObject.layer = LayerMask.NameToLayer("InteractableHidden");
                        }
                        break;
                }
            }
        }
    }

    /*private void OnDrawGizmos()
    {
        if(groundCheckCollider)
        Gizmos.DrawWireSphere(groundCheckCollider.bounds.center, 2f);
    }*/

    public void GrabAndReleaseItem(GameObject itemGrabbed)
    {
        if (!isGrabbingItem)
        {
            GrabItem(itemGrabbed);
        }
        else
        {
            ReleaseItem();
        }

        isGrabbingItem = !isGrabbingItem;

        if (detectionBrain)
            detectionBrain.ActiveStateInteractWhenGrabbingItem();       //Disable interactive detection when is grabbing item
    }

    private void GrabItem(GameObject itemGrabbed)
    {
        grabbedGameObject = itemGrabbed;

        itemParent = grabbedGameObject.transform.parent;
        grabbedGameObject.transform.parent = grabDetect;
        grabbedGameObject.transform.position = (grabDetect.position + new Vector3(facingDirection, 0, 0));     //Put the box slightly infront of the character
        grabbedGameObject.GetComponent<Rigidbody2D>().isKinematic = true;
        grabbedGameObject.layer = LayerMask.NameToLayer("Untouchable");       //Untouchable = 10

        CharacterActionManager.ReduceJumpForce(GetHoldBoxJumpForceMultiplier());
        CharacterActionManager.ReducecMoveSpeed(GetHoldBoxJumpForceMultiplier());

        DisableItemCollider(grabbedGameObject);

        if (grabbedGameObject == CharacterActionManager.ReturnPlayer().gameObject)
        {
            grabbedGameObject.transform.position = grabDetect.position - new Vector3(0f, grabbedGameObject.GetComponent<Collider2D>().bounds.size.y / 3, 0);
            CheckPlayerDirection();
        }
    }

    private void ReleaseItem()
    {
        if (grabbedGameObject == CharacterActionManager.ReturnPlayer().gameObject)
        {
            CheckPlayerDirection();
        }

        grabbedGameObject.transform.parent = itemParent;
        grabbedGameObject.GetComponent<Rigidbody2D>().isKinematic = false;
        grabbedGameObject.transform.position = (grabDetect.position + new Vector3(facingDirection, 0.2f, 0));     //Put the box slightly infront of the character
        grabbedGameObject.layer = LayerMask.NameToLayer("Grabbable");       //Grabbable = 11
        grabbedGameObject.GetComponent<Rigidbody2D>().AddForce(
            CharacterActionManager.ReturnCurrentCharacter().GetRigidbody().velocity/1.5f +
                new Vector2((grabbedGameObject.GetComponent<Rigidbody2D>().mass), 0) *
                CharacterActionManager.ReturnCurrentCharacter().IsFacingRight(),
                    ForceMode2D.Impulse);


        CharacterActionManager.IncreaseJumpForce(GetHoldBoxJumpForceMultiplier());
        CharacterActionManager.IncreaseMoveSpeed(GetHoldBoxJumpForceMultiplier());

        if (grabbedGameObject != CharacterActionManager.ReturnPlayer().gameObject)
            EnabaleItemCollider(grabbedGameObject);
        else
            grabbedGameObject.layer = 8;

        grabbedGameObject = null;
    }

    private void CheckPlayerDirection()
    {
        Vector3 theScale = characterSprite.localScale;
        Vector3 playerScale = CharacterActionManager.ReturnPlayer().GetCharacterSpriteTransform().localScale;

        if (!isGrabbingPlayer)
        {
            if (CharacterActionManager.ReturnPlayer().IsFacingRight() != CharacterActionManager.ReturnCurrentCharacter().IsFacingRight())
            {
                CharacterActionManager.ReturnPlayer().SetFacingDirection(theScale.x);
            }
        }
    }

    void DisableItemCollider(GameObject grabbedItem)
    {
        grabbedItem.GetComponent<Collider2D>().isTrigger = true;
    }
    void EnabaleItemCollider(GameObject grabbedItem)
    {
        grabbedItem.GetComponent<Collider2D>().isTrigger = false;
    }
    IEnumerator PreventSpam()
    {
        pauseInteraction = true;
        yield return new WaitForSeconds(1.5f);
        pauseInteraction = false;
    }

    IEnumerator ReleasePlayerAfterTimer()
    {
        staminaSlider.gameObject.SetActive(true);
        for (float i = grabPlayerDuration; i >= 0; i -= Time.deltaTime)
        {
            if (!isGrabbingPlayer) yield break;

            staminaSlider.value = i / grabPlayerDuration;
            yield return null;
        }

        staminaSlider.gameObject.SetActive(false);
        ReleasePlayer();
    }

    private void ReleasePlayer()
    {
        if (CharacterActionManager.ReturnPlayer().GetCharacterSpriteTransform().localScale.x > 0 && characterSprite.localScale.x < 0)
            CharacterActionManager.ReturnPlayer().SetFacingDirection(1);

        if (CharacterActionManager.ReturnPlayer().GetCharacterSpriteTransform().localScale.x < 0 && characterSprite.localScale.x > 0)
            CharacterActionManager.ReturnPlayer().SetFacingDirection(-1);

        if (CharacterActionManager.ReturnCurrentCharacter().IsMoving())
            OnReleasePlayer.Raise();
        
        OnGrabEvent?.Invoke(grabbedGameObject);
        /*CharacterActionManager.ReturnPlayer().GetRigidbody().velocity =
            new Vector2(CharacterActionManager.ReturnCurrentCharacter().GetRigidbody().velocity.x,
            Mathf.Abs(CharacterActionManager.ReturnCurrentCharacter().GetRigidbody().velocity.x) * 2f);*/

        OnSwitchBackPlayer.Raise();
        isGrabbingPlayer = !isGrabbingPlayer;
        StartCoroutine(PreventSpam());
    }

    private bool GroundMonster() { return (CharacterData.unitType == CharacterInfo.CharacterType.GROUND_UNIT); }
    private bool FlyingMonster() { return (CharacterData.unitType == CharacterInfo.CharacterType.FLYING_UNIT); }
    public bool IsGrabbingItem => isGrabbingItem;
    public bool IsGrabbingPlayer => isGrabbingPlayer;
    public float GetHoldBoxJumpForceMultiplier() { return HoldBoxJumpForceMultiplier; }
}
