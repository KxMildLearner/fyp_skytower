﻿using UnityEngine;
using UnityEngine.Events;

public class MovementController : MonoBehaviour
{
	public CharacterInfo CharacterData;
	private BaseMonsterInfo m_monsterData;
	[SerializeField] PlayerInputSO _inputControl;

	private float m_JumpForce => CharacterData.JumpForce * jumpForceFactor;                        // Amount of force added when the player jumps.
	private float m_TapJumpForcePercent => CharacterData.TapJumpForcePercent;
	private float m_RunSpeed => CharacterData.RunSpeed * moveSpeedFactor;
	private float m_MovementSmoothing => CharacterData.MovementSmoothing;			// How much to smooth out the movement
	private bool m_AirControl => CharacterData.AirControl;							// Whether or not a player can steer while jumping;
	private float m_AirHangTime => CharacterData.AirHangTime;                       // The time that allows player to jump even when it falls off the platform*/
	private float m_JumpBufferTime => CharacterData.JumpBufferTime;

	PhysicsMaterial2D _defaultPhysicsMat => CharacterData.DefaultPhysicsMat;
	PhysicsMaterial2D _coarsePhysicsMat => CharacterData.CoarsePhysicsMat;

	//Factors that modify the above values
	float jumpForceFactor = 1f;
	float moveSpeedFactor = 1f;

	//Jump Attributes -- the given time to jump when press button early or later
	float hangCounter;
	float jumpBufferCount;

	[Header("Collider")]
	[SerializeField] private LayerMask m_WhatIsGround;                          // A mask determining what is ground to the character
	LayerMask groundLayer;
	[SerializeField] private Collider2D m_CeilingCheck;                          // A position marking where to check for ceilings
	[SerializeField] private Collider2D m_GroundCheck;                           // A position marking where to check if the player is grounded.
	[SerializeField] private Transform characterSprite;
	float groundCheckHeight;
	float groundCheckWidth;

	[Header("Particle Effect")]
	[SerializeField] private ParticleSystem runDustFX;

	private bool m_Grounded = true;            // Whether or not the player is grounded.
	private Rigidbody2D m_Rigidbody2D;
	[SerializeField] [Space] bool m_FacingRight;  // For determining which way the player is currently facing.

	//Local variables that no need to be on the inspector
	bool isMoving;
	bool isJumping;
	bool isFalling;
	bool reduceNextJumpHeight;

	Vector3 m_Velocity = Vector3.zero;
	//public animator anim;

	[Header("Events")]
	public UnityEvent OnLandEvent;

	[System.Serializable]
	public class BoolEvent : UnityEvent<bool> { }

    private void Awake()
	{
		//Initialization
		m_Rigidbody2D = GetComponent<Rigidbody2D>();
		groundCheckHeight = m_GroundCheck.bounds.size.y - (m_GroundCheck.bounds.size.y - 0.1f);
		groundCheckWidth = m_GroundCheck.bounds.size.x - 0.05f;

		groundLayer = m_WhatIsGround;

		if (CharacterData.unitType != CharacterInfo.CharacterType.PLAYER)
			m_monsterData = (BaseMonsterInfo)CharacterData;

		if (OnLandEvent == null)
			OnLandEvent = new UnityEvent();

		if (!m_FacingRight)
		{
			Flip();
			m_FacingRight = false;
		}
	}


    private void OnDisable()
    {
		GetRigidbody().sharedMaterial = _coarsePhysicsMat;
		isMoving = false;

		if(CharacterData.unitType != CharacterInfo.CharacterType.FLYING_UNIT)
			DisableRunDustFX();
	}

    private void OnEnable()
    {
		GetRigidbody().sharedMaterial = _defaultPhysicsMat;
	}

	/*private void OnDrawGizmos()
    {
		groundCheckWidth = m_GroundCheck.bounds.size.x - 0.05f;
		groundCheckHeight = m_GroundCheck.bounds.size.y - (m_GroundCheck.bounds.size.y - 0.1f);
		Gizmos.DrawCube(new Vector2(m_GroundCheck.bounds.center.x, m_GroundCheck.bounds.min.y), new Vector2(groundCheckWidth, groundCheckHeight));
    }*/

	void Update()
	{
		// Check player input to move
		GetMovementInput();
	}

    private void FixedUpdate()
	{
		ReturnMovementType();

		if (CharacterData.unitType == CharacterInfo.CharacterType.FLYING_UNIT) return;

		bool wasGrounded = m_Grounded;
		m_Grounded = false;

        float Xcoor = m_GroundCheck.bounds.center.x;
		float Ycoor = m_GroundCheck.bounds.min.y;

		// The player is grounded if a boxcast to the groundcheck position hits anything designated as ground
		Collider2D[] colliders = Physics2D.OverlapBoxAll(new Vector2(Xcoor, Ycoor), new Vector2(groundCheckWidth, groundCheckHeight), 0, m_WhatIsGround);

		for (int i = 0; i < colliders.Length; i++)
		{
			if (colliders[i].gameObject != this.gameObject)
			{
				//Preventing double jump when inside one-way platform
				//Also if the player is moving within the OWP, it would count as not grounded
				if (colliders[i].tag == "OneWayPlatform")
				{
					if (m_Rigidbody2D.velocity.y != 0)
					{
						hangCounter = 0f;
						jumpBufferCount = 0f;
						m_Grounded = false;
						return;
					}
					else
					{
						if (verticalMove == -1)
						{
							m_GroundCheck.enabled = false;
							m_CeilingCheck.enabled = false;
						}
					}
				}

				m_Grounded = true;
				isFalling = false;

				//if player was not grounded before hand, execute the landing event
				if (!wasGrounded)
				{
					OnLandEvent.Invoke();
				}
			}
		}	//forloop
	}

    private void OnTriggerExit2D(Collider2D collision)
    {
		if (collision.gameObject.CompareTag("OneWayPlatform"))
		{
			m_GroundCheck.enabled = true;
			m_CeilingCheck.enabled = true;
		}
	}

    public void Move(float move)
	{
		move *= m_RunSpeed * Time.fixedDeltaTime;

		//only control the player if grounded or airControl is turned on
		if (m_Grounded || m_AirControl)
		{
			// Move the character by finding the target velocity
			Vector3 targetVelocity = new Vector2(move * 10f, m_Rigidbody2D.velocity.y);
			// And then smoothing it out and applying it to the character
			m_Rigidbody2D.velocity = Vector3.SmoothDamp(m_Rigidbody2D.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);

			// If the input is moving the player right and the player is facing left...
			if (move > 0 && !m_FacingRight)
			{
				// ... flip the player.
				//Flip();
				SetFacingDirection(1);
			}
			// Otherwise if the input is moving the player left and the player is facing right...
			else if (move < 0 && m_FacingRight)
			{
				// ... flip the player.
				//Flip();
				SetFacingDirection(-1);
			}

			if (move != 0)
            {
				if (m_Grounded)
					CreateRunDustFX();
				else
					DisableRunDustFX();
            }
			else
				DisableRunDustFX();
		}

		if (hangCounter > 0f && jumpBufferCount > 0f)
        {
            jumpBufferCount = -1f;
            Jump();
        }

        if(!m_Grounded && m_Rigidbody2D.velocity.y < -0.2f)
        {
			isFalling = true;
			isJumping = false;
		}
    }

    public void Jump()
    {
        // Add a vertical force to the player.
        m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x, m_JumpForce);
		isJumping = true;

		if (reduceNextJumpHeight)
        {
            m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x, m_JumpForce * m_TapJumpForcePercent);
            hangCounter = 0;
            reduceNextJumpHeight = false;
        }
    }

	public void AirJump()
	{
		// Add a vertical force to the player.
		m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x, m_JumpForce / 1.5f);
		isJumping = true;
	}

	void FlyingMove(float horizontal, float vertical)
	{
		m_Rigidbody2D.AddForce(new Vector2(horizontal, vertical).normalized * m_RunSpeed * (m_Rigidbody2D.mass / 1) * 10f * Time.fixedDeltaTime);

		if (horizontal > 0 && !m_FacingRight)
			Flip();
		else if (horizontal < 0 && m_FacingRight)
			Flip();

	}

	private void Flip()
	{
		// Switch the way the player is labelled as facing.
		m_FacingRight = !m_FacingRight;

		Vector3 theScale = characterSprite.localScale;
		theScale.x *= -1;
		characterSprite.localScale = theScale;
	}

	private void CreateRunDustFX()
    {
		if (!runDustFX.isPlaying)
		{
			var sfx = runDustFX.main;
			sfx.loop = true;
			runDustFX.Play();
		}
	}

	private void DisableRunDustFX()
    {
		if (runDustFX.isPlaying )
		{
			var sfx = runDustFX.main;
			sfx.loop = false;
			runDustFX.Stop();
		}
	}

	#region GetSet Function

	float horizontalMove = 0f;
	float verticalMove = 0f;
	private void GetMovementInput()
	{
		horizontalMove = _inputControl.GetHorizontalMovement();
		verticalMove = _inputControl.GetVerticalMovement();

		if (horizontalMove != 0)
		{
			isMoving = true;
			CharacterActionManager.isZoomOut = false;
		}
		else
        {
			isMoving = false;
		}

		if (_inputControl.GetJumpInputDown() && !isJumping)
		{
			jumpBufferCount = m_JumpBufferTime;
			CharacterActionManager.isZoomOut = false;
		}

		if (jumpBufferCount > 0)
			jumpBufferCount -= Time.deltaTime;

		if (m_Grounded)
			hangCounter = m_AirHangTime;
		else
			hangCounter -= Time.fixedDeltaTime;

		if (_inputControl.GetJumpInputUp())
		{
			if (m_Rigidbody2D.velocity.y > 0)
			{
				m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x, m_Rigidbody2D.velocity.y * m_TapJumpForcePercent);
				hangCounter = 0;
				return;
			}

			if (jumpBufferCount > 0)
				reduceNextJumpHeight = true;
		}
	}

	private void ReturnMovementType()
	{
		if (CharacterData.unitType != CharacterInfo.CharacterType.FLYING_UNIT) // Movement for normal ground units
		{
			Move(horizontalMove);
			return;
		}

		FlyingMove(horizontalMove, verticalMove);   //Movement explicitly for flying units
	}
	public float IsFacingRight()
	{
		if (m_FacingRight)
			return 1;
		else
			return -1;
	}
	public void SetFacingDirection(float facingRight)
	{
		if(facingRight > 0)
        {
			if (characterSprite.transform.localScale.x < 0)
            {
				Vector3 scale = characterSprite.transform.localScale;
                scale.x = Mathf.Abs(scale.x);
                characterSprite.transform.localScale = scale;
				m_FacingRight = true;
			}
		}
		else
        {
			if (characterSprite.transform.localScale.x > 0)
            {
				Vector3 scale = characterSprite.transform.localScale;
				scale.x = -scale.x;
                characterSprite.transform.localScale = scale;
				m_FacingRight = false;
			}
        }
	}

	public void ResetGroundLayer() { m_WhatIsGround = groundLayer; }
	public float GetJumpFroce() { return m_JumpForce; }
	public void SetJumpForceFactor(float jumpForce) { jumpForceFactor -= jumpForce; }
	public void SetMoveSpeedFactor(float moveSpeed) { moveSpeedFactor -= moveSpeed; }
	public bool IsGrounded() { return m_Grounded; }
	public bool IsMoving() { return isMoving; }
	public bool IsJumping() { return isJumping; }
	public bool IsFalling() { return isFalling; }
	public void SetJumpingState(bool state) { isJumping = state; }
	public bool GroundUnitIsNotGrounded() { return CharacterData.unitType != CharacterInfo.CharacterType.FLYING_UNIT && !m_Grounded; }
	public Transform GetCharacterSpriteTransform() { return characterSprite; }
	public Rigidbody2D GetRigidbody() { return m_Rigidbody2D; }
	public Collider2D GetGroundCheckCollider() { return m_GroundCheck; }
	public Collider2D GetCeilingCheckCollider() { return m_CeilingCheck; }
	public BaseMonsterInfo MonsterInfo() { return m_monsterData; }
	public LayerMask GroundLayer() { return m_WhatIsGround; }
	public void SetGroundLayer(LayerMask newGroundLayers) { m_WhatIsGround = newGroundLayers; }

	#endregion
}
