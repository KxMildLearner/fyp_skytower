﻿using UnityEngine;

public class InteractableController : MonoBehaviour
{
    [SerializeField] BaseInteractable interactableInfo;
    [SerializeField] Animator _anim;

    public BaseInteractable InteractableInfo { get => interactableInfo; set => interactableInfo = value; }

    private void Awake()
    {
        interactableInfo.ResetInitialValue();
    }

    private void Start()
    {
        interactableInfo.SetAnimator(_anim);
    }
}
