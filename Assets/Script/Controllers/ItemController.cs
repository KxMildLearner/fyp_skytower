﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemController : MonoBehaviour
{
    [SerializeField] BaseItemClass itemInfo;

    public BaseItemClass ItemInfo { get => itemInfo; set => itemInfo = value; }
}
