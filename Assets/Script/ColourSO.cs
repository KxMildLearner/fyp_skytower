﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColourSO : ScriptableObject
{
    public Color barrierWhite;
    public Color barrierPink;
    public Color barrierBlue;
    public Color barrierYellow;
}
