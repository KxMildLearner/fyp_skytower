﻿using UnityEngine;

namespace GameSystem.Events
{
    public class TransformEventListener : BaseGameEventListener<Transform, TransformEvent, UnityTransformEvent> { }
}