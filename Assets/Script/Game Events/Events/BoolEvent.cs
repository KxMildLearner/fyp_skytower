﻿using UnityEngine;
using System;

namespace GameSystem.Events
{
    [CreateAssetMenu(fileName = "New Bool Event", menuName = "GameEvents/Bool Event")]
    public class BoolEvent : BaseGameEvent<bool>
    { 
    }
}