using System;
using UnityEngine;

[CreateAssetMenu(fileName = "New Transform Event", menuName = "GameEvents/Transform Event")]
public class TransformEvent : BaseGameEvent<Transform>
{
    
}
