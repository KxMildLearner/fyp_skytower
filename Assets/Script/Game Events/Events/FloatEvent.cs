﻿using UnityEngine;

namespace GameSystem.Events
{
    [CreateAssetMenu(fileName = "New FloatEvent", menuName = "GameEvents/Float Event")]
    public class FloatEvent : BaseGameEvent<float>
    {
    }
}