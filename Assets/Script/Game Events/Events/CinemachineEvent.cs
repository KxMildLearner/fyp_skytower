﻿using UnityEngine;
using Cinemachine;

namespace GameSystem.Events
{
    [CreateAssetMenu(fileName = "New Cinemachine Event", menuName = "GameEvents/Camera Event")]
    public class CinemachineEvent : BaseGameEvent<CinemachineVirtualCamera>
    { 
    }
}