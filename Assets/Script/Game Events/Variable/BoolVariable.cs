﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BoolVariable", menuName = "Variable/Bool")]
public class BoolVariable : BaseVariable<bool>
{
    public void Set(bool value)
    {
        this.RuntimeValue = value;
    }
}
