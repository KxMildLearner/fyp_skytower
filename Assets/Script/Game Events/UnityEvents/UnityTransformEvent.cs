﻿using UnityEngine.Events;
using UnityEngine;

namespace GameSystem.Events
{
    [System.Serializable]
    public class UnityTransformEvent : UnityEvent<Transform> { }
}