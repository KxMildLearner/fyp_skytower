﻿using UnityEngine.Events;
using Cinemachine;

namespace GameSystem.Events
{
    [System.Serializable]
    public class UnityCinemachineEvent : UnityEvent<CinemachineVirtualCamera> {}
}