﻿using UnityEngine;
using UnityEngine.Events;
using System;

namespace GameSystem.Events
{
    [System.Serializable]
    public class UnityBoolEvent : UnityEvent<bool> {}
}