﻿using UnityEngine.Events;

namespace GameSystem.Events
{
    [System.Serializable]
    public class UnityFloatEvent : UnityEvent<float> { }
}