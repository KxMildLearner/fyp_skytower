﻿using UnityEngine;

public class CharacterAnimation : MonoBehaviour
{
    [SerializeField] Animator _anim;

    [Header("Controller Affected")]
    [SerializeField] MovementController characterMovement;
    [SerializeField] GrabItemController grabbingAction;

    bool jumpOnce;
    bool grabOnce;
    
    void Update()
    {
        if(!characterMovement.IsMoving())
        {
            _anim.SetBool("isWalking", false);
        }
        else
        {
            _anim.SetBool("isWalking", true);
        }

        if(characterMovement.IsJumping() && !jumpOnce)
        {
            _anim.SetTrigger("takeOff");
            _anim.SetBool("isJumping", true);
            jumpOnce = true;
        }

        if(characterMovement.IsFalling())
        {
            _anim.SetBool("isFalling", true);
            _anim.SetBool("isJumping", false);
        }
        else
        {
            _anim.SetBool("isFalling", false);
        }

        if(characterMovement.IsGrounded() && !characterMovement.IsJumping() && jumpOnce)
        {
            jumpOnce = false;
        }
        
        if (!grabbingAction)
            return;

        if (grabbingAction.IsGrabbingItem)
        {
            if(!grabOnce)
            {
                _anim.SetTrigger("grab");
                _anim.SetBool("isGrabbing", true);
                grabOnce = true;
            }
        }
        else
        {
            if(grabOnce)
            {
                _anim.SetBool("isGrabbing", false);
                _anim.SetTrigger("putdown");
                grabOnce = false;
            }
        }
    }

    public void PlayLandedAnimation()
    {
        _anim.SetTrigger("landed");
    }

    public void PlayWalkingAnimation()
    {
        _anim.Play("Ana_Walk");
    }

    public void StartControllingAnimation(GameObject none)
    {
        _anim.SetTrigger("startControl");
        _anim.SetBool("isControlling", true);
    }

    public void StopControllingAnimation()
    {
        _anim.SetBool("isControlling", false);
    }

    public void PlayHoldOntoFlyingBirdAnimation(GameObject none)
    {
        _anim.SetTrigger("flyOnce");
        _anim.SetBool("isFlying", true);
    }
    
    public void StopHoldOntoFlyingBirdAnimation()
    {
        _anim.SetBool("isFlying", false);
    }

    public void PlayAngeredAnimation()
    {
        _anim.SetBool("isAngered", true);
    }
}
