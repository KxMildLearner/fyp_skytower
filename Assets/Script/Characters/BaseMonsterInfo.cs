﻿using UnityEngine;

public enum MonsterColour { Pink, Blue, Yellow, None }

[CreateAssetMenu(fileName = "New Monster", menuName = "Character/Monster/Base", order = 1)]
public class BaseMonsterInfo : CharacterInfo
{
    [Header("Monster Attributes")]
    [SerializeField] MonsterColour colour;
    [Range(0, 1f)] [SerializeField] protected float holdBoxMovementPenalty;
    [SerializeField] protected bool limitRangeOfBeingControlled;

    [Header("Rage Attributes")]
    [SerializeField] float rageLimit;
    [SerializeField] float rateOfRage;

    [Header("Flying Monster Exclusive")]
    [SerializeField] protected float grabPlayerRadius;
    [SerializeField] protected float grabPlayerDuration;

    public bool LimitRangeOfBeingControlled { get => limitRangeOfBeingControlled; set => limitRangeOfBeingControlled = value; }
    public float HoldBoxJumpForceReduction { get => holdBoxMovementPenalty; set => holdBoxMovementPenalty = value; }
    public float GrabPlayerRadius { get => grabPlayerRadius; set => grabPlayerRadius = value; }
    public float GrabPlayerDuration { get => grabPlayerDuration; set => grabPlayerDuration = value; }
    public float RageLimit { get => rageLimit; set => rageLimit = value; }
    public float RateOfRage { get => rateOfRage; set => rateOfRage = value; }
    public MonsterColour Colour { get => colour; set => colour = value; }
}
