﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Character", menuName = "Character/Character")]
public class CharacterInfo : ScriptableObject
{
    public enum CharacterType { 
        PLAYER, 
        GROUND_UNIT, 
        FLYING_UNIT 
    };

    public CharacterType unitType;
    [SerializeField] protected string characterName;

    [Header("Movement Attributes")]
    [SerializeField] private float runSpeed;
    [SerializeField] protected float jumpForce;                            // Amount of force added when the player jumps.
    [Range(0f, 1f)] [SerializeField] protected float tapJumpForcePercent;
    [Range(0, .3f)] [SerializeField] protected float movementSmoothing;  // How much to smooth out the movement
    [SerializeField] protected bool airControl = false;                         // Whether or not a player can steer while jumping;
    [SerializeField] protected float airHangTime;                          // The time that allows player to jump even when it falls off the platform
    [SerializeField] protected float jumpBufferTime;

    [Header("Physic Material")]
    [SerializeField] PhysicsMaterial2D defaultPhysicsMat;
    [SerializeField] PhysicsMaterial2D coarsePhysicsMat;

    public float RunSpeed { get => runSpeed; set => runSpeed = value; }
    public float JumpForce { get => jumpForce; set => jumpForce = value; }
    public float TapJumpForcePercent { get => tapJumpForcePercent; set => tapJumpForcePercent = value; }
    public float MovementSmoothing { get => movementSmoothing; set => movementSmoothing = value; }
    public bool AirControl { get => airControl; set => airControl = value; }
    public float AirHangTime { get => airHangTime; set => airHangTime = value; }
    public float JumpBufferTime { get => jumpBufferTime; set => jumpBufferTime = value; }
    public string CharacterName { get => characterName; set => characterName = value; }
    public PhysicsMaterial2D DefaultPhysicsMat { get => defaultPhysicsMat; set => defaultPhysicsMat = value; }
    public PhysicsMaterial2D CoarsePhysicsMat { get => coarsePhysicsMat; set => coarsePhysicsMat = value; }
}
