﻿using UnityEngine;
using FMODUnity;

public class FootstepSound : MonoBehaviour
{
    [SerializeField] [EventRef] string footstep;
    [SerializeField] [EventRef] string jumpSFX;
    [SerializeField] [EventRef] string jumpLandingSFX;

    public void PlayFootstep()
    {
        RuntimeManager.PlayOneShot(footstep);
    }

    public void PlayJumpSFX()
    {
        RuntimeManager.PlayOneShot(jumpSFX);
    }

    public void PlayLandingSFX()
    {
        RuntimeManager.PlayOneShot(jumpLandingSFX); 
    }
}
