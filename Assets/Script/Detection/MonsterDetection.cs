﻿using System.Collections.Generic;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
using GameSystem.Events;
using FMODUnity;

public class MonsterDetection : BaseDetectionClass
{
    [Tooltip("The multiplier for the range when controlling the monster. 1.0 = 100%")]
    [SerializeField] float linkingRadiusFactor;

    [Header("Events")]
    [SerializeField] GameObjectEvent OnCommandMonster;
    [SerializeField] VoidEvent OnSwitchBackPlayer;

    [Header("Particle Effect")]
    [SerializeField] LineRenderer selectionLine;
    [SerializeField] Light2D playerCommandingGlow;
    [SerializeField] float radiusIncrementWhenControl;

    [Header("Sound Effect")]
    [SerializeField] [EventRef] string monsterRoarSFX;
    [SerializeField] [EventRef] string eagleScreamSFX;
    [SerializeField] [EventRef] string lightAmbientSFX;
    FMOD.Studio.EventInstance lightSFX = new FMOD.Studio.EventInstance();

    // Fields for detecting and selecting nearby monster to command
    GameObject selectedMonster;
    GameObject controlledMonster;
    List<GameObject> seenMonsters = new List<GameObject>();
    List<GameObject> monsterInRange = new List<GameObject>();

    [Header("Global Variable")]
    [SerializeField] BoolVariable controllingTarget;
    bool selectingTarget;

    /*void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(OriginTransform.position, detectionRadius);
    }*/

    private void Start()
    {
        controllingTarget.OnAfterDeserialize();
        lightSFX = RuntimeManager.CreateInstance(lightAmbientSFX);
    }

    private void Update()
    {
        SwitchMonsterInRange();     //Switching between multiple available monster
        ControlMonster();           //Control the selected monster
    }

    private void FixedUpdate()
    {
        CheckTargetInRange();
        EmitSelectionParticleFX();
        LinkingDistanceLimit();
    }

    #region Monster Detection Methods
    private void CycleMonstersForward()
    {
        var monsterSelectionIndex = seenMonsters.IndexOf(selectedMonster);
        monsterSelectionIndex++;
        monsterSelectionIndex %= seenMonsters.Count;
        selectedMonster = seenMonsters[monsterSelectionIndex];
    }

    private int ProperModulous(int x, int m) => (x % m + m) % m;

    private void CycleMonstersBackwards()
    {
        var monsterSelectionIndex = seenMonsters.IndexOf(selectedMonster);
        monsterSelectionIndex--;
        monsterSelectionIndex = ProperModulous(monsterSelectionIndex, seenMonsters.Count);
        selectedMonster = seenMonsters[monsterSelectionIndex];
    }

    protected override void CheckTargetInRange()
    {
        #region Archieve CircleCast method
        //Archieve using CircleCast to check for monster in range

        /*RaycastHit2D[] hit = Physics2D.CircleCastAll(transform.position, detectionRadius, new Vector2(0, 0), 8);

        foreach (RaycastHit2D currentMonster in hit)
        {
            var dir = currentMonster.transform.position - PlayerParent.transform.position;

            if (currentMonster.collider.CompareTag("Monster"))
            {
                if (LineOfSight(currentMonster.collider.gameObject, dir))
                {
                    //Check whether the player can see the monster inside detection radius
                    if (!monsterInRange.Contains(currentMonster.collider.gameObject))
                    {
                        monsterInRange.Add(currentMonster.collider.gameObject);

                        if (selectedMonster == null)
                        {
                            selectedMonster = currentMonster.collider.gameObject;
                            monsterSelectionIndex = monsterInRange.IndexOf(selectedMonster);
                        }
                    }
                }
            }
        }


        foreach(GameObject monster in monsterInRange.ToList())
        {
            bool exist = false;
            for (int i=0; i < hit.Length; i++)
            {
                if(monster == hit[i].collider.gameObject)
                {
                    exist = true;
                    break;
                }
            }

            if (!exist)
            {
                monsterInRange.Remove(monster);
                if (!monsterInRange.Contains(selectedMonster))
                {
                    if (monsterInRange.Any())
                    {
                        selectedMonster = monsterInRange.First();

                    }
                    else
                        selectedMonster = null;
                }
                monsterSelectionIndex = monsterInRange.IndexOf(selectedMonster);
            }
        }*/

        #endregion
        #region Archieve OverlapCircle method
        /*Collider2D[] hit = Physics2D.OverlapCircleAll(PlayerParent.transform.position, detectionRadius);

        foreach (Collider2D currentMonster in hit)
        {
            var dir = currentMonster.transform.position - PlayerParent.transform.position;

            if (currentMonster.CompareTag("Monster"))
            {
                if (LineOfSight(currentMonster.gameObject, dir))
                {
                    //Check whether the player can see the monster inside detection radius
                    if (!monsterInRange.Contains(currentMonster.gameObject))
                    {
                        monsterInRange.Add(currentMonster.gameObject);

                        if (selectedMonster == null)
                        {
                            selectedMonster = currentMonster.gameObject;
                            monsterSelectionIndex = monsterInRange.IndexOf(selectedMonster);
                        }
                    }
                }
            }
        }

        foreach (GameObject monster in monsterInRange)
        {
            bool exist = false;
            for (int i = 0; i < hit.Length; i++)
            {
                if (monster == hit[i].gameObject)
                {
                    exist = true;
                    break;
                }
            }

            if (!exist)
            {
                monsterInRange.Remove(monster);
                if (!monsterInRange.Contains(selectedMonster))
                {
                    if (monsterInRange.Any())
                    {
                        selectedMonster = monsterInRange.First();

                    }
                    else
                        selectedMonster = null;
                }
                monsterSelectionIndex = monsterInRange.IndexOf(selectedMonster);
            }
        }*/
        #endregion
        #region Archieve Globol Distance Comparison Method
        //GameObject closestMonster = null;

        /*foreach (GameObject currentMonster in allMonster)
        {
            Vector2 dir = currentMonster.transform.position - PlayerParent.transform.position;
            float distanceToMonster = dir.magnitude;

            if(distanceToMonster > detectionRadius)
            {
                monsterInRange.Remove(currentMonster);
                monsterSelectionIndex = monsterInRange.IndexOf(selectedMonster);
                continue;
            }

            if (distanceToMonster < detectionRadius)
            {
                if (LineOfSight(currentMonster, dir))
                {
                    //Check whether the player can see the monster inside detection radius
                    if (!monsterInRange.Contains(currentMonster))
                    {
                        monsterInRange.Add(currentMonster);

                        if (selectedMonster == null)
                        {
                            selectedMonster = currentMonster;
                            monsterSelectionIndex = monsterInRange.IndexOf(selectedMonster);
                        }
                    }
                    Debug.DrawLine(PlayerParent.transform.position, currentMonster.transform.position, Color.red);
                }
            }

            if(!monsterInRange.Contains(selectedMonster))
            {
                selectedMonster = currentMonster;
                monsterSelectionIndex = monsterInRange.IndexOf(selectedMonster);
            }
        } //end foreach*/
        #endregion
        #region New Optimize Way
        
        monsterInRange.Clear();
        Collider2D[] hit = Physics2D.OverlapCircleAll(OriginCollider.transform.position, detectionRadius, targetLayer);
        foreach (Collider2D currentMonster in hit)
        {
            //Debug.Log(currentMonster);
            if (currentMonster.CompareTag("Monster") &&
                LineOfSight(currentMonster.gameObject))
            {
                monsterInRange.Add(currentMonster.gameObject);

                if (!seenMonsters.Contains(currentMonster.gameObject)) 
                { 
                    seenMonsters.Add(currentMonster.gameObject);
                }

                if(!controllingTarget.RuntimeValue)
                    if (currentMonster.TryGetComponent(out SelectionHighlight selection)) { selection.enabled = true; }
            }
        }

        if (!selectedMonster && seenMonsters.Any())
        {
            selectedMonster = seenMonsters.FirstOrDefault();
            selectingTarget = true;
            return;
        }

        for (int i = 0; i < seenMonsters.Count; ++i)
        {
            var monster = seenMonsters[i];
            if (!monsterInRange.Contains(monster))
            {
                if (monster == selectedMonster) CycleMonstersForward();
                seenMonsters.RemoveAt(i--);

                if (monster.TryGetComponent(out SelectionHighlight selection)) { selection.enabled = false; }
            }
        }

        if (!seenMonsters.Any())    //if there are no monster can be seen
        {
            selectedMonster = null;
            selectingTarget = false;
        }
        #endregion
    }

    private void ControlMonster()
    {
        if(_inputControl.GetCommandMonsterInput() && !CharacterActionManager.pauseInteraction && CharacterActionManager.ReturnCurrentCharacter().IsGrounded())
        {
            if(CharacterActionManager.isPlayer)
            {
                if (selectedMonster)
                {
                    //Command Function
                    controlledMonster = selectedMonster;
                    OnCommandMonster.Raise(controlledMonster);
                    controllingTarget.Set(true);
                    
                    //VFX
                    if(controlledMonster.TryGetComponent(out SelectionHighlight light)) light.enabled = false;
                    StartCoroutine(FadeControlMonsterGlowStart());

                    //SFX
                    if(controlledMonster.GetComponent<MovementController>().MonsterInfo().unitType == CharacterInfo.CharacterType.FLYING_UNIT)
                        RuntimeManager.PlayOneShotAttached(eagleScreamSFX, controlledMonster);
                    else
                        RuntimeManager.PlayOneShotAttached(monsterRoarSFX, controlledMonster);

                    RuntimeManager.AttachInstanceToGameObject(lightSFX, CharacterActionManager.ReturnPlayer().transform);
                    lightSFX.start();
                }
                return;
            }

            if (controlledMonster.GetComponent<GrabItemController>().IsGrabbingPlayer)         //if flying monster is grabbing the player, exit from this method
                return;


            //Switching from Monster back to Player
            OnSwitchBackPlayer.Raise();
            controlledMonster = null;
            controllingTarget.Set(false);

            //VFX 
            StartCoroutine(FadeControlMonsterGlowEnd());

            //SFX
            lightSFX.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        }
    }

    private void SwitchMonsterInRange()
    {
        if (controllingTarget.RuntimeValue) return;

        //Switching within monster in range
        if (_inputControl.GetSwitchSelectedCharacterInput() && seenMonsters.Any())
        {
            CycleMonstersForward();
        }
    }
    #endregion

    #region Linking System
    private void AlignParticleDirection(Transform target)
    {
        selectionLine.enabled = true;
        selectionLine.SetPosition(0, OriginCollider.transform.position);
        selectionLine.SetPosition(1, target.transform.position);
    }

    private void EmitSelectionParticleFX()
    {
        if (controllingTarget.RuntimeValue)
        {
            AlignParticleDirection(controlledMonster.transform);
            return;
        }

        if (selectingTarget)
        {
            AlignParticleDirection(selectedMonster.transform);
            return;
        }

        selectionLine.enabled = false;
    }

    public void ControlMonsterFlowCoroutineStart()
    {
        if (controllingTarget.RuntimeValue) return;

        StartCoroutine(FadeControlMonsterGlowStart());
    }

    public void ControlMonsterFlowCoroutineEnd()
    {
        StartCoroutine(FadeControlMonsterGlowEnd());
    }

    IEnumerator FadeControlMonsterGlowStart()
    {
        float initialOuterRadius = playerCommandingGlow.intensity;
        for (float value = initialOuterRadius; value < (initialOuterRadius + 1f); value += Time.deltaTime)
        {
            playerCommandingGlow.intensity = value;
            yield return null;
        }
    }

    IEnumerator FadeControlMonsterGlowEnd()
    {
        float initialOuterRadius = playerCommandingGlow.intensity;
        for (float value = initialOuterRadius; value > (initialOuterRadius - 1f); value -= Time.deltaTime)
        {
            playerCommandingGlow.intensity = value;
            yield return null;
        }
    }

    SpringJoint2D springJoint;
    private void EnableSpringJointBasedOnDistance(GameObject monster, BaseMonsterInfo monsterInfo)
    {
        var dist = monster.transform.position - OriginCollider.transform.position;
        float maximumControllingRange = detectionRadius * linkingRadiusFactor;

        if (dist.magnitude <= maximumControllingRange)
        {
            springJoint.enabled = false;
        }
        else
        {
            springJoint.enabled = true;
            springJoint.distance = maximumControllingRange - 1f;
            springJoint.connectedAnchor = OriginTransform.position;
        }
    }
    private void LinkingDistanceLimit()
    {
        if (controllingTarget.RuntimeValue)
        {
            var monsterInfo = controlledMonster.GetComponent<MovementController>().MonsterInfo();

            if (monsterInfo.LimitRangeOfBeingControlled)
            {
                controlledMonster.TryGetComponent(out SpringJoint2D _sJoint);
                springJoint = _sJoint;

                EnableSpringJointBasedOnDistance(controlledMonster, monsterInfo);
            }
            return;
        }
    }
    #endregion

    public void SetAsControlledMonster(GameObject thisMonster)
    {
        controlledMonster = thisMonster;
    }    
}
