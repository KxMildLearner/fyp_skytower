﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseDetectionClass : MonoBehaviour
{
    //This class specify what are the basic components are needed before focus on detecting what type of objects
    [SerializeField] protected PlayerInputSO _inputControl;

    [Header("Detection Attributes")]
    [Tooltip("The origin of the detection started from. (Choose between Transform or Collider")]
    [SerializeField] protected Transform OriginTransform;
    [SerializeField] protected Collider2D OriginCollider = null;

    [Tooltip("The layers specifically to detect in the detection radius.")]
    [SerializeField] protected LayerMask targetLayer;

    [Tooltip("The layers specifically to ignore for LOS raycast to check for the target.")]
    [SerializeField] protected LayerMask ignoreLayer;

    [SerializeField] protected float detectionRadius;

    protected bool LineOfSight(GameObject target)
    {
        float Xcoor, Ycoor;
        RaycastHit2D raycastTarget;
        var dir = target.transform.position - OriginCollider.transform.position;

        if (OriginCollider)
        {
            Xcoor = (dir.x < 0) ? OriginCollider.bounds.min.x - 0.1f : OriginCollider.bounds.max.x + 0.1f;
            Ycoor = OriginCollider.bounds.max.y;
            dir = target.transform.position - new Vector3(Xcoor, Ycoor);

            raycastTarget = Physics2D.Raycast(new Vector2(Xcoor, Ycoor), dir, detectionRadius, ~ignoreLayer);
            //Debug.DrawRay(new Vector2(Xcoor, Ycoor), dir, Color.black);

            if (!raycastTarget.collider) return false;

            //Debug.Log("Raycast Target : " + raycastTarget.collider.gameObject);
            //Debug.Log("Target : " + target);
            return raycastTarget.collider.gameObject == target;
        }

        Xcoor = OriginTransform.position.x;
        Ycoor = OriginTransform.position.y;

        raycastTarget = Physics2D.Raycast(new Vector2(Xcoor, Ycoor), dir, Mathf.Infinity, ~ignoreLayer);
        //Debug.DrawRay(new Vector2(Xcoor, Ycoor), dir);
        return raycastTarget.collider.gameObject == target;
    }

    protected abstract void CheckTargetInRange();
}
