﻿using UnityEngine;
using UnityEngine.UI;
using GameSystem.Events;

public class MonsterRageDetection : BaseDetectionClass
{
    [Header("Rage Attributes")]
    [SerializeField] BaseMonsterInfo monsterInfo;
    private float rageLimit => monsterInfo.RageLimit;
    private float rateOfRage => monsterInfo.RateOfRage;
    
    [SerializeField] Slider rageMeterUI;
    [SerializeField] Image monsterEyeIcon;
    [SerializeField] VoidEvent OnMonsterAngered;

    float rageMeter = 0;
    bool isAngered;

    private void Start()
    {
        rageMeterUI.value = 0;
    }
    
    void Update()
    {
        CheckTargetInRange();
        UpdateRageMeter();
        //Debug.DrawRay(OriginTransform.position, Vector2.right * transform.lossyScale, Color.white);
    }

    protected override void CheckTargetInRange()
    {
        RaycastHit2D lineOfSight = Physics2D.Raycast(OriginTransform.position, Vector2.right * transform.lossyScale, detectionRadius, targetLayer);

        bool objectIsMonster = (lineOfSight.collider != null && lineOfSight.collider.tag == "Monster");

        if (objectIsMonster)
        {
            if(lineOfSight.collider.TryGetComponent(out MovementController monsterUnit))
            {
                var raycastedMonsterInfo = (BaseMonsterInfo)monsterUnit.CharacterData;

                //If the monster are in different colour, increase rage
                if (monsterInfo.Colour != raycastedMonsterInfo.Colour)
                {
                    Vector2 distanceBetweenMonsters = lineOfSight.transform.position - transform.position;
                    if (rageMeter >= rageLimit && !isAngered)
                    {
                        rageMeter = rageLimit;
                        GetComponent<CharacterAnimation>().PlayAngeredAnimation();
                        OnMonsterAngered.Raise();
                        isAngered = true;
                        return;
                    }
                    rageMeter += (Time.deltaTime * rateOfRage) * (Mathf.Exp(detectionRadius / distanceBetweenMonsters.magnitude));
                }
                else
                {
                    //If the monster are in same colour, reduce rage
                    if (rageMeter > 0)
                    {
                        rageMeter -= (Time.deltaTime * rateOfRage);
                        return;
                    }
                    rageMeter = 0;
                }
            }            
        }
        else
        {
            //If there is no monster in sight, reduce rage
            if (rageMeter > 0)
            {
                rageMeter -= (Time.deltaTime * rateOfRage);
                return;
            }
            rageMeter = 0;
        }
    }

    void UpdateRageMeter()
    {
        if(rageMeter > 0)
        {
            rageMeterUI.gameObject.SetActive(true);
            rageMeterUI.value = rageMeter / rageLimit;
        }
        else
        {
            rageMeterUI.gameObject.SetActive(false);
        }

        float ratio = 1 - (rageMeter / rageLimit);
        monsterEyeIcon.color = new Color(1, ratio, ratio);
    }
}
