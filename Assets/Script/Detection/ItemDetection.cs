﻿using UnityEngine;
using FMODUnity;

public class ItemDetection : MonoBehaviour
{
    [SerializeField] Inventory _inventory;
    [SerializeField] [EventRef] string CollectItemSFX;

    //Check for pickable items
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out ItemController item))
        {
            if (_inventory.FindItem(item.ItemInfo))
                return;

            switch (item.ItemInfo.ItemType)
            {
                case ItemTypeList.Key:
                    var key = item.ItemInfo;
                    _inventory.AddItem(key);
                    RuntimeManager.PlayOneShot(CollectItemSFX);
                    Destroy(collision.gameObject);
                    break;
            }
        }
    }
}
