﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectionBehaviour : MonoBehaviour
{
    [SerializeField] GrabItemController grabController;
    [SerializeField] InteractableDetection interactationController;

    public void ActiveStateInteractWhenGrabbingItem()
    {
        if(grabController.IsGrabbingItem)
        {
            interactationController.enabled = false;
        }
        else
        {
            interactationController.enabled = true;
        }
    }
}
