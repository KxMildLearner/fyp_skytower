﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InteractableDetection : BaseDetectionClass
{
    [Space]
    [SerializeField] CharacterInfo _characterInfo;
    [SerializeField] Inventory _inventory;
    [SerializeField] InteractableType targetableInteract;
    [SerializeField] Transform characterSprite;

    InteractableController selectedInteractable = null;
    InteractableController cachedSelectedInteractable = null;

   /* private void OnDrawGizmosSelected()
    {
        //Gizmos.DrawWireSphere(transform.position, detectionRadius);
        Gizmos.DrawLine(OriginCollider.transform.position, Vector2.right * characterSprite.lossyScale);
    }*/

    private void Update()
    {
        InteractableTypeChecker();


        //Debug.DrawRay(OriginCollider.transform.position, Vector2.right * transform.lossyScale, Color.white);
        //Debug.Log(targetableInteract);
    }

    private void FixedUpdate()
    {
        CheckTargetInRange();
    }

    private void CycleSelectedForward(List<GameObject> detectedDoorList)
    {
        var selectionIndex = detectedDoorList.IndexOf(selectedInteractable.gameObject);
        selectionIndex++;
        selectionIndex %= detectedDoorList.Count;
        selectedInteractable = detectedDoorList[selectionIndex].GetComponent<InteractableController>();
    }

    //Check Interactable Objects
    /*protected override void CheckTargetInRange()
    {
        interactableInRange.Clear();
        Collider2D[] hit = Physics2D.OverlapCircleAll(OriginCollider.transform.position, detectionRadius, targetLayer);
        float closestDistance = Mathf.Infinity;

        foreach (Collider2D detectedInteractable in hit)
        {
            var dir = detectedInteractable.transform.position - OriginCollider.transform.position;
            float distanceToItem = dir.magnitude;

            if(distanceToItem < closestDistance)
            {
                closestDistance = distanceToItem;

                if (detectedInteractable.TryGetComponent(out InteractableController interactable) &&
                LineOfSight(detectedInteractable.gameObject, dir))
                {
                    interactableInRange.Add(detectedInteractable.gameObject);
                    selectedInteractable = interactable;
                }
            }
        }

        if(!interactableInRange.Any()) { selectedInteractable = null; return; }

        if(!interactableInRange.Contains(selectedInteractable.gameObject)) CycleSelectedForward(interactableInRange);
    }*/

    protected override void CheckTargetInRange()
    {
        RaycastHit2D hit = Physics2D.Raycast(OriginCollider.transform.position, Vector2.right * characterSprite.lossyScale, detectionRadius, targetLayer);

        if (hit.collider && hit.collider.gameObject != this.gameObject)
        {
            if (hit.collider.gameObject.TryGetComponent(out InteractableController interactable))
            {
                cachedSelectedInteractable = selectedInteractable = interactable;
                return;
            }
        }

        selectedInteractable = null;

        if(cachedSelectedInteractable != selectedInteractable) { if (cachedSelectedInteractable.TryGetComponent(out SelectionHighlight selection)) { selection.enabled = false; }}
    }

    void InteractableTypeChecker()
    {
        if (!selectedInteractable) return;

        //May want to call the "unable to intearct" action of the objects instead of just return back the method
        if ((targetableInteract & selectedInteractable.InteractableInfo.Type) == 0) return;

        if (selectedInteractable.TryGetComponent(out SelectionHighlight selection)) { selection.enabled = true; }

        switch (selectedInteractable.InteractableInfo.Type)
        {
            case InteractableType.Door:
                InteractWithDoor();
                break;
            case InteractableType.Mechanism:
                InteractWithMechanism();
                break;
            case InteractableType.Barrier:
                InteractWithSwitch();
                break;
        }
    }


    #region Interaction
    void InteractWithDoor()
    {
        if (_inputControl.GetInteractInput())
        {
            var door = (DoorController)selectedInteractable.InteractableInfo;
            if (door.DoorIsLocked)
            {
                var keyExist = _inventory.FindItemsOfType(ItemTypeList.Key);

                if (keyExist.Any())
                {
                    for (int i = 0; i < keyExist.Count; ++i)
                    {
                        var key = (KeyItem)keyExist[i];

                        //Unlock Door
                        if (key.CheckKeyFitDoor(door))
                        {
                            door.UnlockDoor();
                            selectedInteractable.GetComponent<Collider2D>().enabled = false;
                            _inventory.RemoveItem(key);
                            return;
                        }
                    }
                }

                //Cannot Open Door
                door.NoKeyToUnlock();
            }
        }
    }

    void InteractWithMechanism()
    {
        if (_inputControl.GetInteractInput())
        {
            var mechanism = (MechanismController)selectedInteractable.InteractableInfo;

            if (_characterInfo.unitType == CharacterInfo.CharacterType.PLAYER)
            {
                mechanism.PlaFailTriggerAnimation();
                return;
            }

            if (_characterInfo.unitType == CharacterInfo.CharacterType.GROUND_UNIT)
            {
                var characterInfo = (BaseMonsterInfo)_characterInfo;

                if (characterInfo.Colour == mechanism.MatchingColour)
                {
                    mechanism.OpenConnectedDoor();
                    selectedInteractable.GetComponent<Collider2D>().enabled = false;
                }
                else
                {
                    //If colour is wrong, need to have graphical feedback and sound SFX
                    mechanism.PlaFailTriggerAnimation();
                }
            }
        }
    }

    void InteractWithSwitch()
    {
        if (_inputControl.GetInteractInput())
        {
            var barrierSwitch = (BarrierSwitchController)selectedInteractable.InteractableInfo;

            barrierSwitch.ActivateSwitch();
        }
    }
    #endregion
}
