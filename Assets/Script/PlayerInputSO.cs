﻿using UnityEngine;
public class PlayerInputSO : ScriptableObject
{
	public float GetHorizontalMovement() { return Input.GetAxisRaw("Horizontal"); }
	public float GetVerticalMovement() { return Input.GetAxisRaw("Vertical"); }
	public bool GetJumpInputDown() { return Input.GetButtonDown("Jump"); }
	public bool GetJumpInputUp() { return Input.GetButtonUp("Jump"); }
	public bool GetCommandMonsterInput() { return Input.GetButtonDown("Command Character"); }
	public bool GetSwitchSelectedCharacterInput() { return Input.GetButtonDown("Switch Character"); }
	public bool GetWorldCameraInput() { return Input.GetButtonDown("Zoom Camera"); }
	public bool GetGrabInput() { return Input.GetButtonDown("Interact"); }
	public bool GetInteractInput() { return Input.GetButtonDown("Interact"); }
	public bool GetRestartInput() { return Input.GetButtonDown("Restart"); }
	public bool GetContinueInput() { return Input.GetButtonDown("Continue"); }
	public bool GetPauseInput() { return Input.GetButtonDown("Cancel"); }
}
