﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxBehaviour : MonoBehaviour
{
	[SerializeField] Transform m_GroundCheck;
	[SerializeField] LayerMask m_WhatIsGround;
	BoxCollider2D _collider;
    
	bool isGrounded;

    public bool IsGrounded { get => isGrounded; set => isGrounded = value; }

    private void Start()
    {
		_collider = GetComponent<BoxCollider2D>();
    }

    private void FixedUpdate()
    {
        CheckGround();
    }

    private void CheckGround()
    {
        if (Time.frameCount % 3 == 0)
        {
            isGrounded = false;
            Collider2D[] colliders = Physics2D.OverlapBoxAll(m_GroundCheck.position, _collider.bounds.size - new Vector3(_collider.bounds.size.x * 0.1f, _collider.bounds.size.y * 0.9f), 0, m_WhatIsGround);

            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].gameObject != gameObject)
                {
                    isGrounded = true;
                }
            }
        }
    }
}
