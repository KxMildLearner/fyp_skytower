﻿using System;
using UnityEngine;

[Flags] public enum InteractableType
{ 
    Nothing = 0, 
    Everything = 0b1111, 
    Door = 1, 
    Mechanism = 2,
    Barrier = 4
}

[CreateAssetMenu(fileName = "New Interactable", menuName = "Interactable/Normal")]
public abstract class BaseInteractable : ScriptableObject
{
    [SerializeField] protected InteractableType type;
    protected Animator _anim;

    public InteractableType Type { get => type; set => type = value; }
    public void SetAnimator(Animator animator)
    {
        _anim = animator;
    }

    public abstract void ResetInitialValue();
}
