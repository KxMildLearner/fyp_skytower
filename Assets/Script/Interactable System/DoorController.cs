﻿using UnityEngine;
using FMODUnity;

[CreateAssetMenu(fileName = "New Door", menuName = "Interactable/Door")]
public class DoorController: BaseInteractable, ISerializationCallbackReceiver
{
    [SerializeField] bool locked;
    [SerializeField] [EventRef] string OpenDoorSFX;
    [SerializeField] [EventRef] string DoorLockSFX;

    bool isLocked;
    bool isOpened;
    public void UnlockDoor()
    {
        isLocked = false;
        InteractDoor();
    }

    public void InteractDoor()
    {
        if(!isOpened)
        {
            _anim.SetTrigger("open");
            RuntimeManager.PlayOneShot(OpenDoorSFX);
            isOpened = !isOpened;
        }
    }

    public void NoKeyToUnlock()
    {
        RuntimeManager.PlayOneShot(DoorLockSFX);
    }

    public bool DoorIsLocked => isLocked;

    public override void ResetInitialValue()
    {
        isLocked = locked;
        isOpened = false;
    }

    public void OnAfterDeserialize()
    {
        isLocked = locked;
        isOpened = false;
        _anim = null;
    }

    public void OnBeforeSerialize() { type = InteractableType.Door; }
}
