﻿using UnityEngine;
using FMODUnity;

[CreateAssetMenu(fileName = "New Mechanism", menuName = "Interactable/Mechanism")]
public class MechanismController : BaseInteractable, ISerializationCallbackReceiver
{
    [SerializeField] DoorController connectedDoor;
    [SerializeField] MonsterColour matchingColour;
    [SerializeField] [EventRef] string SuccessSFX;
    [SerializeField] [EventRef] string FailSFX;

    public MonsterColour MatchingColour { get => matchingColour; set => matchingColour = value; }

    public void OpenConnectedDoor()
    {
        if(connectedDoor.DoorIsLocked)
        {
            connectedDoor.UnlockDoor();
            _anim.SetTrigger("trigger");
            RuntimeManager.PlayOneShot(SuccessSFX);
        }
    }

    public void PlaFailTriggerAnimation()
    {
        _anim.SetTrigger("failTrigger");
        RuntimeManager.PlayOneShot(FailSFX);
    }

    public override void ResetInitialValue() {}

    public void OnAfterDeserialize() { }
    public void OnBeforeSerialize() { type = InteractableType.Mechanism; }
}
