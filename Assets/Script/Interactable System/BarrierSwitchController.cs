﻿using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

[CreateAssetMenu(fileName = "new Barrier Switch", menuName = "Interactable/Barrier Switch")]
public class BarrierSwitchController : BaseInteractable, ISerializationCallbackReceiver
{
    List<BarrierController> barrierList = new List<BarrierController>();
    [SerializeField] [EventRef] string ButtonPressedSFX;

    public void AddBarrier(BarrierController barrier)
    {
        barrierList.Add(barrier);
    }

    public void ActivateSwitch()
    {
        _anim.SetTrigger("pressed");
        RuntimeManager.PlayOneShot(ButtonPressedSFX);
        foreach(BarrierController barrier in barrierList)
        {
            barrier.NextState();
        }
    }

    public override void ResetInitialValue()
    {
        barrierList.Clear();
    }

    public void OnAfterDeserialize() { barrierList.Clear(); }
    public void OnBeforeSerialize() { type = InteractableType.Barrier; }
}
