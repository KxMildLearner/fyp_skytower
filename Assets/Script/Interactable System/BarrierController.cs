﻿using UnityEngine;
using System.Collections.Generic;

public class BarrierController : MonoBehaviour
{
    [Header("Barrier Colour")]
    [SerializeField] ColourSO colourPalette;

    [Header("Barrier Attributes")]
    [SerializeField] CharacterInfo.CharacterType passableCharacter;
    [SerializeField] MonsterColour matchingColour;
    [SerializeField] BoxCollider2D blockingCollider;
    [SerializeField] SpriteRenderer _barrierSprite;
    [SerializeField] List<BarrierSwitchController> masterSwitch;

    [Header("Pattern Sequence")]
    [SerializeField] List<MonsterColour> colourSequence;

    LayerMask groundLayerWithoutBarrier = 1 << 0 | 1 << 11;
    List<MovementController> inBoundCharacter = new List<MovementController>();

    int index;

    private void Start()
    {
        colourSequence.Insert(0, matchingColour);

        if(masterSwitch.Count != 0)
        {
            foreach(BarrierSwitchController switches in masterSwitch)
            {
                switches.AddBarrier(this);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.TryGetComponent(out MovementController character))
        {
            inBoundCharacter.Add(character);

            if (character.CharacterData.unitType != passableCharacter) return;

            if (character.CharacterData.unitType == CharacterInfo.CharacterType.GROUND_UNIT)
            {
                var monster = collision.GetComponent<MovementController>();

                if (monster.MonsterInfo().Colour != matchingColour) return;
            }

            MakeCharacterPassable(character);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out MovementController character))
        {
            inBoundCharacter.Remove(character);

            if (character.CharacterData.unitType != passableCharacter) return;
            if (character.CharacterData.unitType == CharacterInfo.CharacterType.GROUND_UNIT)
            {
                var monster = collision.GetComponent<MovementController>();

                if (monster.MonsterInfo().Colour != matchingColour) { return; }
            }

            MakeCharacterUnpassable(character);
        }
    }

    public void NextState()
    {
        ChangeBarrierColour(colourSequence[(++index) % colourSequence.Count]);

        foreach(MovementController character in inBoundCharacter)
        {
            if (character.CharacterData.unitType == CharacterInfo.CharacterType.GROUND_UNIT)
            {
                if (character.MonsterInfo().Colour == matchingColour)
                {
                    MakeCharacterPassable(character);
                    return;
                }
            }

            if (character.CharacterData.unitType == CharacterInfo.CharacterType.PLAYER && matchingColour == MonsterColour.None)
            {
                MakeCharacterPassable(character);
                return;
            }

            MakeCharacterUnpassable(character);
        }
    }

    private static void MakeCharacterUnpassable(MovementController character)
    {
        character.GetComponent<MovementController>().ResetGroundLayer();
        character.GetGroundCheckCollider().gameObject.layer = LayerMask.NameToLayer("Character");
        character.GetCeilingCheckCollider().gameObject.layer = LayerMask.NameToLayer("Character");
    }

    private void MakeCharacterPassable(MovementController character)
    {
        character.GetComponent<MovementController>().SetGroundLayer(groundLayerWithoutBarrier);
        character.GetGroundCheckCollider().gameObject.layer = LayerMask.NameToLayer("BarrierCharacter");
        character.GetCeilingCheckCollider().gameObject.layer = LayerMask.NameToLayer("BarrierCharacter");
    }

    public void ChangeBarrierColour(MonsterColour monsterColour)
    {
        matchingColour = monsterColour;
        
        switch(matchingColour)
        {
            case MonsterColour.Blue:
                passableCharacter = CharacterInfo.CharacterType.GROUND_UNIT;
                _barrierSprite.color = colourPalette.barrierBlue;
                break;
            case MonsterColour.Pink:
                passableCharacter = CharacterInfo.CharacterType.GROUND_UNIT;
                _barrierSprite.color = colourPalette.barrierPink;
                break;
            case MonsterColour.Yellow:
                passableCharacter = CharacterInfo.CharacterType.GROUND_UNIT;
                _barrierSprite.color = colourPalette.barrierYellow;
                break;
            case MonsterColour.None:
                passableCharacter = CharacterInfo.CharacterType.PLAYER;
                _barrierSprite.color = colourPalette.barrierWhite;
                break;
        }
    }
}
