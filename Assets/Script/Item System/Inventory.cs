﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    private List<BaseItemClass> itemList = new List<BaseItemClass>();

    public bool FindItem(BaseItemClass itemCheck)
    {
        foreach(BaseItemClass _itemInstance in itemList)
        {
            if (_itemInstance == itemCheck)
                return true;
        }
        return false;
    }

    public List<BaseItemClass> FindItemsOfType(ItemTypeList type)
    {
        List<BaseItemClass> list = new List<BaseItemClass>();
        foreach (BaseItemClass _itemInstance in itemList)
        {
            if (_itemInstance.ItemType.Equals(type))
            {
                list.Add(_itemInstance);
            }
        }
        return list;
    }

    public void AddItem(BaseItemClass newItem)
    {
        itemList.Add(newItem);
    }

    public void RemoveItem(BaseItemClass deleteItem)
    {
        itemList.Remove(deleteItem);
    }
}