﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New key", menuName = "Item/Key")]
public class KeyItem: BaseItemClass
{
    [SerializeField] private DoorController unlockDoor;

    public DoorController UnlockDoor { get => unlockDoor; set => unlockDoor = value; }

    public bool CheckKeyFitDoor(DoorController doorFound)
    {
        return (doorFound == unlockDoor);
    }
}
