﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemTypeList { 
    Box, 
    Key, 
}

[CreateAssetMenu(fileName = "New item", menuName = "Item/Normal", order = 0)]
public class BaseItemClass : ScriptableObject
{
    [SerializeField] private string itemName;
    [SerializeField] private ItemTypeList itemType;
    [SerializeField] private bool stackable;

    public string ItemName { get => itemName; set => itemName = value; }
    public ItemTypeList ItemType { get => itemType; set => itemType = value; }
    public bool Stackable { get => stackable; set => stackable = value; }
}
