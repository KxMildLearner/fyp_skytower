﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeriodicAlpha : MonoBehaviour
{
    [SerializeField] SpriteRenderer _sprite;
    [SerializeField] [Range(0, 255f)] int minAlpha;
    [SerializeField] [Range(0, 255f)] int maxAlpha;
    [SerializeField] [Range(0, 1)] float alteringRate;

    float normalizedMin;
    float normalizedMax;

    Color _spriteColor;
    bool change;

    private void Start()
    {
        _spriteColor = _sprite.color;
        normalizedMin = minAlpha / 255f;
        normalizedMax = maxAlpha / 255f;
    }

    private void Update()
    {
        if (_spriteColor.a <= normalizedMax && !change)
        {
            _spriteColor.a += Time.deltaTime * alteringRate;
        }

        if(_spriteColor.a >= normalizedMin && change)
        {
            _spriteColor.a -= Time.deltaTime * alteringRate;
        }

        if (_spriteColor.a >= normalizedMax)
            change = true;
        if (_spriteColor.a <= normalizedMin)
            change = false;

        _sprite.color = _spriteColor;
    }
}
