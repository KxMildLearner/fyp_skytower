﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionHighlight : MonoBehaviour
{
    [SerializeField] GameObject selectionOutline;

    private void OnDisable()
    {
        selectionOutline.SetActive(false);
    }

    private void OnEnable()
    {
        selectionOutline.SetActive(true);
    }
}
