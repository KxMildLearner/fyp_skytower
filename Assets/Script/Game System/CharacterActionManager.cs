﻿using System.Collections;
using UnityEngine;
using Cinemachine;


public class CharacterActionManager : MonoBehaviour
{
    [SerializeField] private PlayerInputSO PlayerInput;
    [SerializeField] private BoolVariable isPlayerCommandingTarget;

    [Header("Camera Setup")]
    [SerializeField] private CinemachineBrain cameraBrain;
    [SerializeField] private CinemachineVirtualCamera playerVirtualCamera;
    [SerializeField] private CinemachineVirtualCamera selectedMonsterVirtualCamera;
    [SerializeField] private CinemachineVirtualCamera introTransitionCamera;
    private CinemachineVirtualCamera areaFocusCamera;
    
    //Current character controlling
    private MovementController[] MoveableCharacters;
    private static MovementController player;
    private static MovementController currentCharacter;

    public static bool isPlayer;
    public static bool isZoomOut;
    bool initial = true;
    bool doOnce;

    // Start is called before the first frame update
    void Start()
    {
        isPlayer = true;
        isZoomOut = true;
        isPlayerCommandingTarget.Set(false);
        areaFocusCamera = introTransitionCamera;

        MoveableCharacters = GameObject.FindObjectsOfType<MovementController>();

        foreach (MovementController characters in MoveableCharacters)
        {
            //Turn off certain scripts that are not needed when is not the active char
            //OnOffPlayerInputScripts(characters, false);

            switch (characters.CharacterData.unitType)
            {
                case CharacterInfo.CharacterType.PLAYER:
                    player = currentCharacter = characters;
                    break;
                case CharacterInfo.CharacterType.GROUND_UNIT:
                    break;
                case CharacterInfo.CharacterType.FLYING_UNIT:
                    break;
            }
        }

        //StartCoroutine(IntroTransition());      //Prevent player input while the scene is transitioning
    }

    // Update is called once per frame
    void Update()
    {
        if(initial && !doOnce)
        {
            if(Input.GetKeyDown(KeyCode.Space) || !SceneLoader.currentLevelFirstTime)
            {
                doOnce = true;
                StartCoroutine(IntroTransition());      //Prevent player input while the scene is transitioning
            }
        }

        TransToOverviewCam();
    }

    /*private void FixedUpdate()
    {
        SetPlayerStaticToXAxis();
    }*/

    #region Camera Control
        private void TransToOverviewCam()
        {
            if (PlayerInput.GetWorldCameraInput())
            {
                isZoomOut = !isZoomOut;
            }

            //if isZoomOut is true, set Priority = 5, else = 0
            areaFocusCamera.Priority = (isZoomOut) ? 10 : 0;
        }

        public void UpdateAreaFocusCam(CinemachineVirtualCamera camera)
        {
            areaFocusCamera = camera;
        }

        public static void CancelZoomOutCam()
        {
            isZoomOut = false;
        }

        public void MonsterCamFollow(GameObject monster)
        {
            playerVirtualCamera.Priority = 0;
            selectedMonsterVirtualCamera.Priority = 1;
            selectedMonsterVirtualCamera.Follow = monster.transform;
        }

        public void PlayerCamFollow()
        {
            selectedMonsterVirtualCamera.Priority = 0;
            playerVirtualCamera.Priority = 1;
        }

        IEnumerator IntroTransition()
        {
            introTransitionCamera.Priority = 0;
            isZoomOut = false;
            yield return new WaitUntil(() => cameraBrain.IsBlending);
            yield return new WaitUntil(() => !cameraBrain.IsBlending);
            OnOffPlayerInputScripts(player, true);
            initial = false;
        }
    #endregion

    #region Monster Control
        public static bool pauseInteraction;
        public void CommandMonster(GameObject selectedMonster)
        {
            //if (currentCharacter.characterType == PlayerControl.CharacterType.GROUND_MONSTER && currentCharacter.GetGrabController().IsGrabbingBox())
            //  return;

            if (pauseInteraction || currentCharacter.CharacterData.unitType != CharacterInfo.CharacterType.PLAYER)   //if it's currently preventing interaction, exit from this function
                return;

            //Disable the current character movement
            currentCharacter.GetRigidbody().velocity = Vector2.zero;
            OnOffPlayerInputScripts(currentCharacter, false);

            if (isPlayer)
            {
                //player.GetSpriteRenderer().color = new Color(0.4f, 0.63f, 0.74f);

                //INITIALISATION -- Swap the new character into control
                currentCharacter = selectedMonster.GetComponent<MovementController>();
            }

            OnOffPlayerInputScripts(currentCharacter, true);
            isPlayer = false;
        }
        public void CommandMonsterWithDelay(GameObject selectedMonster)
        {
            //if (currentCharacter.characterType == PlayerControl.CharacterType.GROUND_MONSTER && currentCharacter.GetGrabController().IsGrabbingBox())
            //  return;

            if (pauseInteraction)   //if it's currently preventing interaction, exit from this function
                return;

            //Disable the current character movement
            currentCharacter.GetRigidbody().velocity = Vector2.zero;
            OnOffPlayerInputScripts(currentCharacter, false);

            if (isPlayer)
            {
                //player.GetSpriteRenderer().color = new Color(0.4f, 0.63f, 0.74f);

                //INITIALISATION -- Swap the new character into control
                currentCharacter = selectedMonster.GetComponent<MovementController>();
            }

            //Prevent Switching Character Spam enabled
            StartCoroutine(SwitchCharacterCooldown());
            isPlayerCommandingTarget.Set(true);
        }

        public void SwitchBackPlayer()
        {
            if (pauseInteraction)   //if it's currently preventing interaction, exit from this function
                return;

            //Disable the current character movement
            currentCharacter.GetRigidbody().velocity = Vector2.zero;
            OnOffPlayerInputScripts(currentCharacter, false);

            //INITIALISATION -- If the monster is the current character, switch back to player
            currentCharacter = player;

            //CAMERA SWAP
            selectedMonsterVirtualCamera.Priority = 0;
            playerVirtualCamera.Priority = 1;
            selectedMonsterVirtualCamera.Follow = null;

            //Enable current character movement
            OnOffPlayerInputScripts(currentCharacter, true);
            isPlayer = true;
        }
        public void SwitchBackPlayerWithDelay()
        {
            if (pauseInteraction)   //if it's currently preventing interaction, exit from this function
                return;

            //Disable the current character movement
            currentCharacter.GetRigidbody().velocity = Vector2.zero;
            OnOffPlayerInputScripts(currentCharacter, false);

            //INITIALISATION -- If the monster is the current character, switch back to player
            currentCharacter = player;

            //CAMERA SWAP
            selectedMonsterVirtualCamera.Priority = 0;
            playerVirtualCamera.Priority = 1;
            selectedMonsterVirtualCamera.Follow = null;

            //Enable character movement and avoid spamming
            StartCoroutine(SwitchCharacterCooldown());
            isPlayerCommandingTarget.Set(false);
        }

        private void OnOffPlayerInputScripts(MovementController characters, bool onOff)
        {
            characters.enabled = onOff;
            if (characters.TryGetComponent(out InteractableDetection interactDetection)) { interactDetection.enabled = onOff; }
            if (characters.TryGetComponent(out GrabItemController grabController))
            {
                if (characters.CharacterData.unitType == CharacterInfo.CharacterType.FLYING_UNIT) return;
                grabController.enabled = onOff;
            }

            if(characters.CharacterData.unitType == CharacterInfo.CharacterType.PLAYER)
            {
                if(initial)
                    characters.GetComponentInChildren<MonsterDetection>().enabled = true;

                characters.GetComponentInChildren<InteractableDetection>().enabled = onOff;
                characters.GetComponentInChildren<ItemDetection>().enabled = onOff;
            }
        }

        IEnumerator SwitchCharacterCooldown()
        {
            pauseInteraction = true;
            yield return new WaitForSeconds(0.5f);
            pauseInteraction = false;

            //Enable the current character movement
            OnOffPlayerInputScripts(currentCharacter, true);
            isPlayer = !isPlayer;
        }
    #endregion

    #region GetSet Function
        public static void ReduceJumpForce(float reductionNormalized)
        {
            currentCharacter.SetJumpForceFactor(1 - reductionNormalized);
        }
        public static void IncreaseJumpForce(float incrementNormalized)
        {
            currentCharacter.SetJumpForceFactor(incrementNormalized - 1);
        }
        public static void ReducecMoveSpeed(float reductionNormalized)
        {
            currentCharacter.SetMoveSpeedFactor(1 - reductionNormalized);
        }
        public static void IncreaseMoveSpeed(float incrementNormalized)
        {
            currentCharacter.SetMoveSpeedFactor (incrementNormalized - 1);
        }
        public static MovementController ReturnPlayer()
        {
            return player;
        }
        public static MovementController ReturnCurrentCharacter()
        {
            return currentCharacter;
        }
        public MovementController[] GetMoveableCharacters()
        {
            return MoveableCharacters;
        }
    #endregion
}
