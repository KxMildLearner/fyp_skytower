﻿using System.Collections;
using UnityEngine;

public class TooltipTrigger : MonoBehaviour
{
    [SerializeField] Animator _anim;
    [SerializeField] [TagSelector] string eligibleTags;
    [SerializeField] float requiredTimeTrigger = 3f;
    bool once;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag(eligibleTags) && !once)
        {
            StartCoroutine(TriggerTooltipTimer());
            once = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag(eligibleTags) && once)
        {
            _anim.SetTrigger("exit");
            once = false;
        }
    }

    IEnumerator TriggerTooltipTimer()
    {
        for (float i = 0f; i <= requiredTimeTrigger; i += Time.deltaTime)
        {
            yield return null;
        }

        _anim.SetTrigger("trigger");
    }
}
