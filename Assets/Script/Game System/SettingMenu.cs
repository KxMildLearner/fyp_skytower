﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingMenu : MonoBehaviour
{
    FMOD.Studio.Bus master;
    FMOD.Studio.Bus bgm;
    FMOD.Studio.Bus sfx;

    private void Start()
    {
        master = FMODUnity.RuntimeManager.GetBus("bus:/Main");
        bgm = FMODUnity.RuntimeManager.GetBus("bus:/Main/Music");
        sfx = FMODUnity.RuntimeManager.GetBus("bus:/Main/SFX");
    }

    public void BGMSetVolume(float volume)
    {
        bgm.setVolume(DecibeltoLinear(volume));
    }

    public void MasterSetVolume(float volume)
    {
        master.setVolume(DecibeltoLinear(volume));
    }
    
    public void SFXSetVolume(float volume)
    {
        sfx.setVolume(DecibeltoLinear(volume));
    }

    public void SetQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    float DecibeltoLinear(float dB)
    {
        float Linear = Mathf.Pow(10.0f, dB / 20f);
        return Linear;
    }
}
