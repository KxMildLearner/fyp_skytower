﻿using System.Collections;
using UnityEngine;
using UnityEngine.Playables;
using GameSystem.Events;
using FMODUnity;

public class LevelExitPoint : MonoBehaviour
{
    [SerializeField] PlayerInputSO _inputControl;
    [SerializeField] VoidEvent OnCompleteLevel;
    [SerializeField] VoidEvent OnLoadNextLevel;
    [SerializeField] PlayableDirector director;

    [SerializeField] [EventRef] string WinningSFX;

    GameObject player;
    Transform playerTransform;
    Transform doorTransform;

    bool clickOnce;
    bool exited;
    bool playerDetected;
    bool movingTowardDoor;

    private void Start()
    {
        doorTransform = transform;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            playerDetected = true;
            player = collision.gameObject;
            playerTransform = collision.transform;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            playerDetected = false;
        }
    }

    private void Update()
    {
        if(playerDetected)
        {
            if(_inputControl.GetJumpInputDown() && !clickOnce)
            {
                OnCompleteLevel.Raise();
                director.Play();

                movingTowardDoor = true;
                clickOnce = true;
            }
        }
    }

    private void FixedUpdate()
    {
        if (movingTowardDoor && !exited)
        {
            if (playerTransform.position.x > doorTransform.position.x && player.GetComponent<MovementController>().IsFacingRight() == 1)
            {
                player.GetComponent<MovementController>().SetFacingDirection(-1);
            }

            if (playerTransform.position.x < doorTransform.position.x && player.GetComponent<MovementController>().IsFacingRight() == -1)
            {
                player.GetComponent<MovementController>().SetFacingDirection(1);
            }

            if (player.transform.position.x != doorTransform.position.x)
            {
                player.transform.position = Vector2.MoveTowards(player.transform.position, new Vector3(doorTransform.position.x, player.transform.position.y), 1.5f * Time.fixedDeltaTime);
            }
            else
            {
                StartCoroutine(CompleteLevelMethod());
                exited = true;
            }
        }
    }

    IEnumerator CompleteLevelMethod()
    {
        yield return new WaitForSeconds(0.75f);
        player.SetActive(false);        // Disable player movement

        yield return new WaitForSeconds(1f);
        OnLoadNextLevel.Raise();
    }
}
