﻿using UnityEngine;
using GameSystem.Events;
using Cinemachine;

public class FocusCamTriggerZone : MonoBehaviour
{
    [SerializeField] CinemachineVirtualCamera focusCam;
    [SerializeField] CinemachineEvent OnUpdateFocusCam;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            OnUpdateFocusCam.Raise(focusCam);
        }
    }
}
