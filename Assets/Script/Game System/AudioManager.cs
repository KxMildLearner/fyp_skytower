﻿using UnityEngine;
using FMODUnity;
using FMOD;

public class AudioManager : MonoBehaviour
{
    [SerializeField] [EventRef] string GameBGM;
    [SerializeField] [EventRef] string LevelSFX;
    [SerializeField] GameObject MainMenuBGM;
    FMOD.Studio.EventInstance music = new FMOD.Studio.EventInstance();
    void Start()
    {
        music = RuntimeManager.CreateInstance(GameBGM);
    }

    public void GameBGMStart()
    {
        RESULT r = music.getPaused(out bool isPaused);

        if (isPaused)
        {
            GameBGMResume();
        }
        else
        {
            music.start();
            MainMenuBGM.SetActive(false);
        }
    }

    public void GameBGMPause()
    {
        music.setPaused(true);
    }

    public void GameBGMResume()
    {
        music.setPaused(false);
    }

    public void GameBGMStop()
    {
        music.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        MainMenuBGM.SetActive(true);
    }
    
    public void LevelSuccessSFX()
    {
        FMOD.Studio.EventInstance sfx = RuntimeManager.CreateInstance(LevelSFX);
        RuntimeManager.AttachInstanceToGameObject(sfx, Camera.main.transform);
        sfx.setParameterByName("LevelStatus", 0);
        sfx.start();
    }

    public void LevelFailSFX()
    {
        FMOD.Studio.EventInstance sfx = RuntimeManager.CreateInstance(LevelSFX);
        sfx.setParameterByName("LevelStatus", 1);
        sfx.start();
    }
}
