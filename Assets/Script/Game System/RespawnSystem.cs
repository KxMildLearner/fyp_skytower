﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RespawnSystem : MonoBehaviour
{
    static protected Vector3 playerRespawnPosition;
    protected bool playerReachedCheckpoint;

    protected List<GameObject> objectsAbleToRespawn = new List<GameObject>();
    protected List<Vector3> objectsStartPosition = new List<Vector3>();

    public void UpdateRespawnPosition(Transform newRespawnPoint)
    {
        playerReachedCheckpoint = true;
        playerRespawnPosition = newRespawnPoint.position;
    }

    public void RespawnTargetOnStartingPosition(GameObject target, int index)
    {
        target.transform.position = objectsStartPosition[index];

        if (target.CompareTag("Player"))
        {
            target.GetComponent<MovementController>().SetFacingDirection(1);
        }

    }

    public void RespawnPlayerOnNewCheckpoint(GameObject target)
    {
        target.transform.position = playerRespawnPosition;
    }
}
