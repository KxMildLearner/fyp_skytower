﻿using UnityEngine;

public class GoalMarker : MonoBehaviour
{
    [SerializeField] Transform player;
    [SerializeField] Transform destination;

    [SerializeField] [Range(0, 1f)] float minScale;
    [SerializeField] [Range(0, 1f)] float maxScale;

    [SerializeField] Animator _anim;

    //Class Variable
    Transform pointer;
    Vector3 relativePos;
    float distanceToDestination;

    private void Start()
    {
        pointer = transform;
        distanceToDestination = (player.position - destination.position).magnitude;

        if(SceneLoader.levelIndex == 1)
        {
            _anim.SetTrigger("initiate");
        }
    }

    private void FixedUpdate()
    {
        relativePos = player.position - destination.position;

        float angleDiff = (Mathf.Atan2(relativePos.y, relativePos.x) * Mathf.Rad2Deg) % 360;
        pointer.rotation = Quaternion.Euler(0,0, angleDiff);
        
        float scaleRatio = Mathf.Clamp(minScale / (relativePos.magnitude / distanceToDestination), minScale, maxScale);
        pointer.localScale = new Vector3(scaleRatio, scaleRatio, 1);
    }
}
