﻿using System.Collections.Generic;
using UnityEngine;
using GameSystem.Events;

public class RespawnTriggerZone : RespawnSystem
{
    [SerializeField] List<string> tagsToDetect;

    private void Start()
    {
        for (int i = 0; i < tagsToDetect.Count; i++)
        {
            objectsAbleToRespawn.AddRange(GameObject.FindGameObjectsWithTag(tagsToDetect[i]));
        }

        for(int i = 0; i < objectsAbleToRespawn.Count; i++)
        {
            objectsStartPosition.Add(objectsAbleToRespawn[i].transform.position);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        bool canRespawn = false;

        for(int i = 0; i < tagsToDetect.Count; i++)
        {
            if (collision.CompareTag(tagsToDetect[i]))
            {
                canRespawn = true;
                break;
            }
        }

        if(canRespawn)
        {
            //OnRespawnPlayer.Raise(collision.gameObject);

            if(playerReachedCheckpoint)
            {
                RespawnPlayerOnNewCheckpoint(collision.gameObject);
                return;
            }

            RespawnTargetOnStartingPosition(collision.gameObject, objectsAbleToRespawn.IndexOf(collision.gameObject));
        }
    }
}
