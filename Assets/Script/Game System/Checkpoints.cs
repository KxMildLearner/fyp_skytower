﻿using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
using FMODUnity;

public class Checkpoints : RespawnSystem
{
    [SerializeField] TransformEvent OnUpdateCheckpoint;

    [SerializeField] Light2D checkpointLight;
    [SerializeField] ParticleSystem checkpointVFX;
    [SerializeField] [EventRef] string checkpointSFX;
    Animator _anim;

    private void Start()
    {
        _anim = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            checkpointLight.pointLightInnerRadius = 0.5f;
            checkpointVFX.Play();
            _anim.SetTrigger("trigger");
            OnUpdateCheckpoint.Raise(this.transform);
            UpdateRespawnPosition(this.transform);
        }
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.R) && playerReachedCheckpoint)
        {
            RespawnPlayerOnNewCheckpoint(CharacterActionManager.ReturnPlayer().gameObject);
        }
    }

    public void PlayCheckpointSound()
    {
        RuntimeManager.PlayOneShot(checkpointSFX);
    }
}
