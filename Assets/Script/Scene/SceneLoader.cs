﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using GameSystem.Events;
using TMPro;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] PlayerInputSO _inputControl;
    [SerializeField] VoidEvent OnStartingGame;
    [SerializeField] VoidEvent OnStoppingGame;

    [Header("Main Menu UI")]
    [SerializeField] GameObject mainMenuCanva;
    [SerializeField] GameObject mainMenuCamera;
    [SerializeField] GameObject mainMenuBackground;

    [Header("Loading UI")]
    [SerializeField] GameObject loadingScreen;
    [SerializeField] Image loadingBackground;
    [SerializeField] Slider loadingSlider;
    [SerializeField] TextMeshProUGUI loadingText;

    [Header("GUI")]
    [SerializeField] GameObject pauseMenu;
    [SerializeField] TextMeshProUGUI levelNumber;
    [SerializeField] GameObject settingUI;
    [SerializeField] GameObject gameOverScreen;

    public static int levelIndex;
    bool inGame;
    bool isLoading;
    bool isFading = false;

    bool touchCheckpoint;

    public static bool currentLevelFirstTime = true; //used in CharacterActionManager to check whether it will automatically start camera transition

    private void Update()
    {
        if(_inputControl.GetRestartInput() && inGame && !touchCheckpoint)
        {
            RestartLevel();
        }

        if (_inputControl.GetPauseInput() && inGame)
        {
            PauseGame();
        }

        if(loadingScreen.activeInHierarchy && !isLoading)
        {
            if(_inputControl.GetContinueInput())
            {
                //loadingScreen.SetActive(false);
                StartCoroutine(FadeOutLoadingScreen());
            }
        }
    }

    public void NextLevelAsync()
    {
        StartCoroutine(FadeInLoadingScreen());
        StartCoroutine(LoadNextLevelAsync());
    }

    public void SelectLevel(int index)
    {
        levelIndex = index - 1;
        StartCoroutine(FadeInLoadingScreen());
        StartCoroutine(SelectLevelAsync());
    }

    public void RestartLevel()
    {
        StartCoroutine(RestartLevelAsync());
    }

    public void UnloadLevel()
    {
        //Check is coming from Game Menu or from previous levels
        if (inGame) OnStoppingGame.Raise();

        inGame = false;
        settingUI.SetActive(false);
        SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().buildIndex + (levelIndex));
        levelIndex = 0;
    }

    public void GameOver()
    {
        gameOverScreen.SetActive(true);
        inGame = false;
        GetComponent<Animator>().SetTrigger("gameOver");
    }

    #region Scene Loading Coroutine
    IEnumerator LoadNextLevelAsync()
    {
        while (isFading)
            yield return null;

        if ((SceneManager.GetActiveScene().buildIndex + (levelIndex + 1)) != 9)
        {
            //Initialisation of a level
            OnStartingGame.Raise();
            currentLevelFirstTime = true;
            LockCursor();
        }
        else
        {
            UnlockCursor();
        }

        ++levelIndex;
        isLoading = true;
        inGame = true;

        AsyncOperation unloadProgress = SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().buildIndex + (levelIndex - 1));
        AsyncOperation progress = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + levelIndex, LoadSceneMode.Additive);

        while(!unloadProgress.isDone)
        {
            yield return null;
        }

        
        while (!progress.isDone)
        {
            float loadingProgress = Mathf.Clamp01(progress.progress / 0.9f);

            loadingSlider.value = loadingProgress;
            loadingText.text = loadingProgress * 100f + " %";

            if (loadingProgress == 1)
            {
                //Loading Screen
                loadingText.text = "Press SPACE to Continue";
                levelNumber.text = "Level " + levelIndex;
                isLoading = false;
            }

            yield return null;
        }
    }

    IEnumerator SelectLevelAsync()
    {
        while (isFading)
            yield return null;

        if ((SceneManager.GetActiveScene().buildIndex + (++levelIndex)) != 9)
        {
            //Initialisation of a level
            OnStartingGame.Raise();
            currentLevelFirstTime = true;
            LockCursor();
        }

        isLoading = true;
        inGame = true;

        AsyncOperation progress = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + levelIndex, LoadSceneMode.Additive);

        while (!progress.isDone)
        {
            float loadingProgress = Mathf.Clamp01(progress.progress / 0.9f);

            loadingSlider.value = loadingProgress;
            loadingText.text = loadingProgress * 100f + " %";

            if (loadingProgress == 1)
            {
                //Loading Screen
                loadingText.text = "Press SPACE to Continue";
                levelNumber.text = "Level " + levelIndex;
                isLoading = false;
            }

            yield return null;
        }
    }

    IEnumerator RestartLevelAsync()
    {
        LockCursor();
        //OnStoppingGame.Raise();
        gameOverScreen.SetActive(false);
        inGame = true;

        AsyncOperation unloadProgress = SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().buildIndex + (levelIndex));
        AsyncOperation progress = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + levelIndex, LoadSceneMode.Additive);

        while (!unloadProgress.isDone)
        {
            yield return null;
        }

        while (!progress.isDone)
        {
            yield return null;
        }

        OnStartingGame.Raise();
        currentLevelFirstTime = false;
    }
    #endregion

    IEnumerator FadeInLoadingScreen()
    {
        if ((SceneManager.GetActiveScene().buildIndex + (levelIndex + 1)) != 9)
        {
            loadingScreen.SetActive(true);
            isFading = true;

            var loadingScreenAlpha = loadingBackground.color;
            loadingText.alpha = 1f;
            loadingScreenAlpha.a = 0f;

            for (float i = 0; i <= 1f; i += Time.deltaTime)
            {
                loadingScreenAlpha.a = i;
                loadingBackground.color = loadingScreenAlpha;
                yield return null;
            }
        }

        isFading = false;
        mainMenuCanva.SetActive(false);
        mainMenuCamera.SetActive(false);
        mainMenuBackground.SetActive(false);
        loadingSlider.gameObject.SetActive(true);
        loadingText.gameObject.SetActive(true);
    }

    IEnumerator FadeOutLoadingScreen()
    {
        var loadingScreenAlpha = loadingBackground.color;
        loadingSlider.gameObject.SetActive(false);
        settingUI.SetActive(true);

        for (float i = loadingScreenAlpha.a; i >= 0f; i -= Time.deltaTime)
        {
            loadingScreenAlpha.a = i;
            loadingBackground.color = loadingScreenAlpha;
            loadingText.alpha = i;
            yield return null;
        }

        loadingScreen.SetActive(false);
    }

    public void CheckpointPresent(bool check) { touchCheckpoint = check; }

    bool isPaused;
    public void PauseGame()
    {
        if(!isPaused)
        {
            pauseMenu.SetActive(true);
            Time.timeScale = 0f;

            if(inGame && (SceneManager.GetActiveScene().buildIndex + (levelIndex + 1)) != 9) UnlockCursor();
        }
        else
        {
            pauseMenu.SetActive(false);
            Time.timeScale = 1f;

            if(inGame && (SceneManager.GetActiveScene().buildIndex + (levelIndex + 1)) != 9) LockCursor();
        }

        isPaused = !isPaused;
    }

    public void LockCursor()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    void UnlockCursor()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void ChangeLevelIndex(int number)
    {
        levelIndex = number;
    }

    public void SettingMenuBackBtn()
    {
        if (levelIndex == 0)
        {
            mainMenuCanva.SetActive(true);
        }
        else
        {
            pauseMenu.SetActive(true);
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
